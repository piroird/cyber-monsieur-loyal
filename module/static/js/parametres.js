// ferme tous les contextmenu de la page
function fermer(){
    let listecontextemenu = document.getElementsByClassName("context-menu");
    Array.from(listecontextemenu).forEach( contextmenu => {
        contextmenu.style.display = "none";
    });
}

//change le modal de suppression en fonction de idApplication
function modifierModalsSupr(idApplication){
    let baliseLienSupr = $("#modalSuprApplication a");
    let lien = $(baliseLienSupr).attr("href");
    let newLien = lien.substring(0, lien.length-2)+idApplication+"/";
    $(baliseLienSupr).attr("href",newLien);
}

// la liste des boutons application
let listeApplication = Array.from( $(".applicationsActuelles"));

// On supprime le contexteMenu lorsque le bouton pert le "focus".
document.addEventListener("click", function(event){
    if(!listeApplication.includes(event.target)){
        if(event.target != document.querySelector(".context-menu")){
            fermer();
        }
    }
});

// Applique à toutes les icons d'application un contextmenu personnalisé
listeApplication.forEach( elem => {
    elem.addEventListener("click", function(event){
        // affichage du contextmenu correspondant
        if(event.currentTarget == elem){
            let idcontextemenu = elem.getAttribute("idapplication");
            let contextemenu = document.getElementById("CM-" + idcontextemenu);
            if(contextemenu.style.display == "block"){
                contextemenu.style.display = "none";
            }
            else {
                fermer();
                contextemenu.style.display = "block";
            }
        }
        // modification des modals de modification et de suppression
        modifierModalsSupr(elem.getAttribute("idapplication"));
    });
});
