#### HEAD ##############
# Class   : Jouer
# Content : Formulaire de lancement d'un match


#### IMPORT ############
from flask_wtf import FlaskForm
from wtforms import SelectField


#### CLASS #############
class Jouer(FlaskForm):
    equipe = SelectField(
        "Equipes",
        choices = list()
    )

    carte = SelectField(
        "Carte",
        choices = list()
    )

    def setup_equipes(self, listeEquipe):
        """
            recupere la liste des equipe

            :param self: le formulaire Jouer
            :param listeEquipe: la liste des equipes
            :type self: Object
            :type listeEquipe: List
        """
        for position in listeEquipe:
            for equipe in listeEquipe[position]:
                self.equipe.choices.append((equipe.idE, equipe.nomE))

    def setup_cartes(self, dicoCartes):
        """
            recupere la liste des Cartes

            :param self: le formulaire Jouer
            :param dicoCartes: la liste des cartes
            :type self: Object
            :type dicoCartes: List
        """
        if dicoCartes is not None:
            for nom, fichier in dicoCartes.items():
                self.carte.choices.append((fichier, nom))
