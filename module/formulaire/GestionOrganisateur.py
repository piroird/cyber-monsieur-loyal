#### HEAD ##############
# Class   : GestionOrganisateur
# Content : Formulaire pour la suppression et adhésion d'un nouvel organisateur


#### IMPORT ############
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Length
from wtforms import StringField, validators, PasswordField, BooleanField


#### CLASS #############
class GestionOrganisateur(FlaskForm):
        username = StringField(
            "username",
            validators = [
                DataRequired("S'il vous plaît, entrer le nom de l'administrateur."),
                Length(min=3, max=50, message="Le nom du joueur doit comporter 3 caratères minimum et 50 maximum.")
            ]
        )
        password = PasswordField(
            "password",
            validators = [
                DataRequired("S'il vous plaît, entrer le mot de passe de l'administrateur."),
                Length(min=8, max=50, message="Le nom du joueur doit comporter 8 caratères minimum et 50 maximum.")
            ]
        )
