#### HEAD ##############
# Service : Models
# Content : Regroupes toutes les méthodes complexes lié à la base
#           de données.


#### IMPORT ############
from .models import *
from .app import login_manager
from datetime import date, datetime
from random import choice
from math import ceil
from copy import copy

#### METHODES ##########
@login_manager.user_loader
def load_user(username):
    """
        retourne l'utilisateur qui a pour nom username

        :param username: nom d'utilisateur
        :type username: String

        :return: l'organisateur qui possede le nom username
        :rtype: Object
    """
    return Organisateur.query.get(username)

def getFichiersCartes(idConcours):
    return os.listdir(get_concours(idConcours).carte) # Insérer +get_concours(idConcours).carte

def getEquipeSortScore(concours):
    """
    Renvoie la liste des equipe triees par leurs scores

    :param concours: le concours
    :type concours: Object

    :return: La liste des equipes triees par leurs scores
    :rtype: List
    """
    res = concours.equipes
    res.sort(key=lambda x:-scoreTotal(concours,x))
    return res

def getEquipeMatchSortScore(concours,idM):
    """
    Renvoie la liste des equipe triees par leurs scores

    :param concours: le concours
    :param idM: l'id du match
    :type concours: Object
    :type idM: int

    :return: La liste des equipes d'un match triees par leurs scores
    :rtype: List
    """
    effectues =  sorted(get_Match(idM).effectues, reverse=True, key=lambda e : e.score)
    res = [ get_Equipe(e.idE) for e in effectues]
    return res

def scoreTotal(concours,equipe):
    """
    Renvoie le score total d'une equipe

    :param concours: le concours
    :param equipe: l'equipe
    :type concours: Object
    :type equipe: Object

    :return: Renvoie le score total d'une equipe
    :rtype: int
    """
    res = 0
    for e in equipe.effectues:
        res+=e.score
    return res


def get_prochain_concours():
    """
        Retourne la liste des prochain concours triée dans
        l'ordre croissant des dates.

        :return: La liste des concours
        :rtype: list
    """
    return Concours.query.filter(Concours.dateDeb>date.today()).order_by(Concours.dateDeb).all()


def get_archives_concours():
    """
        Retourne la liste des concours archivé (terminé) triée
        dans l'ordre décroissant des dates.

        :return: La liste des concours
        :rtype: list
    """
    return Concours.query.filter(Concours.dateDeb<date.today()).order_by(Concours.dateDeb.desc()).all()


def changer_Theme(organisateur, idTheme):
    """
        Change la couleur de thème de l'organisateur

        :param organisateur: L'organisateur actuel
        :param idTheme: L'identifiant qui référence le thème
        :type organisateur: Object
        :type idTheme: int
    """
    organisateur.idTheme = idTheme
    db.session.commit()


def choisir_equipes(concours):
    """
        selectionne les équipes pour le prochain match

        :param concours: Le concours
        :type concours: Object

        :return: equipe participante et non participante
        :rtype: dict
    """
    equipes = dict()
    equipes["participe"] = list()
    equipes["participe_pas"] = list()

    # Si c'est le premier match de chaque équipe
    reste = 0
    if concours.nbrEquipeParMatch=="" or concours.nbrEquipeParMatch==0:
        nbMatchEquipe = 1
    else:
        nbMatchEquipe = concours.nbrEquipeParMatch
    if(len(concours.equipes) % nbMatchEquipe != 0):
        reste = 1
    if(len(concours.matchs) < (len(concours.equipes) // nbMatchEquipe) + reste):
        equipes["participe"], equipes["participe_pas"] = choisir_equipes_premiere_pool(concours)
    # Si chaque équipe à participé à au moins 1 match
    else:
        equipes["participe"], equipes["participe_pas"] = choisir_equipes_autres_pool(concours)
    return equipes


def choisir_equipes_premiere_pool(concours):
    """
        retourne les equipes participantes et non participantes de la première
        pool d'un concours

        :param concours: Le concours
        :type concours: Object

        :return: equipe participante et non participante
        :rtype: double de listes
    """
    equipesparticipantes = list()
    equipesnonparticipantes = list()
    equipesnonparticipantes = copy(concours.equipes)
    potentielparticiper = copy(equipesnonparticipantes)

    # Si c'est le premier match de la première pool.
    if(len(concours.matchs) == 0):
        while(len(equipesparticipantes) != concours.nbrEquipeParMatch and len(equipesnonparticipantes) != 0):
            equipesparticipantes.append(equipesnonparticipantes.pop(equipesnonparticipantes.index(choice(equipesnonparticipantes))))
    # Si ce n'est pas le premier match de la première pool.
    else:
        for match in concours.matchs:
            for effectuer in match.effectues:
                potentielparticiper.remove(get_Equipe(effectuer.idE))
        while(len(equipesparticipantes) != concours.nbrEquipeParMatch and len(potentielparticiper) != 0):
            equipe = potentielparticiper.pop(potentielparticiper.index(choice(potentielparticiper)))
            equipesparticipantes.append(equipe)
            equipesnonparticipantes.remove(equipe)
    return (equipesparticipantes, equipesnonparticipantes)


def choisir_equipes_autres_pool(concours):
    """
        retourne les equipes participantes et non participantes des pool
        (sauf la premières) d'un concours

        :param concours: Le concours
        :type concours: Object

        :return: equipe participante et non participante
        :rtype: double de listes
    """
    equipesparticipantes = list()
    equipesnonparticipantes = list()
    potentielparticipants = list()
    annuaireequipe = dict()
    nbmatchmin = 1
    equipesnonparticipantes = copy(concours.equipes)

    for match in concours.matchs:
        for effectue in match.effectues:
            equipe = get_Equipe(effectue.idE)

            # Si c'est le premier match de l'équipe
            if(equipe.nomE not in annuaireequipe):
                annuaireequipe[equipe.nomE] = { "nbMatch":1, "score":effectue.score, "objet":equipe }

            # Si l'équipe à fait au moins un match
            else:
                annuaireequipe[equipe.nomE]["nbMatch"] += 1
                annuaireequipe[equipe.nomE]["score"] += effectue.score
    nbmatchmin = min([equipe["nbMatch"] for equipe in annuaireequipe.values()])
    for equipe in annuaireequipe.values():
        if(equipe["nbMatch"] == nbmatchmin):
            potentielparticipants.append((equipe["objet"], equipe["score"]))
    if(len(potentielparticipants) <= concours.nbrEquipeParMatch):
        for equipe, _ in potentielparticipants:
            try:
                equipesparticipantes.append(equipe)
                equipesnonparticipantes.remove(equipe)
            except Exception as e:
                pass
    else:
        potentielparticipants = sorted(potentielparticipants, reverse=True, key=lambda x: x[1])
        while(len(equipesparticipantes) != concours.nbrEquipeParMatch and len(potentielparticipants) != 0):
            equipe = potentielparticipants.pop(0)[0]
            equipesparticipantes.append(equipe)
            equipesnonparticipantes.remove(equipe)
    return (equipesparticipantes, equipesnonparticipantes)


def enregistrer_resultats(resultats, idMatch, concours):
    """
        Enregistre en BD le résultat du match.

        :param resultats: Le résultat du match effectué
        :param numeroMatch: L'identifaint du match effectué
        :param concours: le concours
        :type resultats: dict
        :type numeroMatch: int
        :type concours: Object
    """
    match = get_Match(idMatch)
    participations = match.effectues
    bonnereception = True
    for p in participations:
        equipe = get_Equipe(p.idE)
        if(equipe.nomE in resultats):
            p.score = resultats[equipe.nomE]
        else:
            bonnereception = False
    if(bonnereception):
        match.phase = "Terminé"
    db.session.commit()


def get_concours_en_cours():
    """
        retourne le concours actuel

        :return: le concours actuel
        :rtype: Object
    """
    today = datetime.now()
    allconcours = getAll_Concours()
    concoursActuel = list()
    for concours in allconcours:
        if(concours.dateDeb<=today and concours.dateFin>=today):
            concoursActuel.append(concours)
    return concoursActuel

def activer(idApplication):
    """
    change l'application de la plateforme
    """
    listeAppli = getAll_Application()
    length = len(listeAppli)

    appPassee = -1
    appVoulu = -1
    i = 0

    while (i<length and not(appPassee!= -1 and appVoulu!= -1)):
        if listeAppli[i].idA == idApplication :
            appVoulu = i
        if listeAppli[i].active == 1 :
            appPassee = i
        i+=1
    if (i<=length and appPassee != appVoulu):
        listeAppli[appPassee].active = 0
        listeAppli[appVoulu].active = 1
        db.session.commit()
