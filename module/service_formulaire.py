#### HEAD ##############
# Service : Formulaire
# Content : contient les fonctions de modificatios et d'insertions
#           de formulaires dans la bd.


#### IMPORT ############
from .app import app, db
from .formulaire import *
from .service_models import *
from werkzeug.utils import secure_filename
import datetime
import os
import json
from io import StringIO
import subprocess


#### METHODES ##########
def upload_fichier(fichier, repertoire,nomF):
    """
        retourne et upload dans le repertoire:repertoire le fichier:fichier avec pour nom:nomF

        :param fichier: fichier quelconque
        :param repertoire: le chemin du repertoire
        :param nomF: le nom que vous voulez donnez au fichier
        :type fichier: Bytes
        :type repertoire: String
        :type nomF: String

        :return: le nom du fichier
        :rtype: String
    """
    try:
        file = fichier
        filename = secure_filename(nomF)
        file.save("module/static/" + repertoire + filename)
    except:
        filename = None
    return filename

def upload_image(avatar, repertoire):
    """
        retourne et upload dans le repertoire:repertoire le fichier:avatar

        :param fichier: fichier a sauvegarder
        :param repertoire: le chemin du repertoire
        :type fichier: Bytes
        :type repertoire: String

        :return: le nom du fichier
        :rtype: String
    """
    try:
        file = avatar
        filename = secure_filename(file.filename)
        file.save("module/static/" + repertoire + filename)
    except:
        filename = None
    return filename

def newNomPdf(nomC):
    """
        retourne un String normalisé de la date pour le changement du nom du PDF

        :param nomC: Le nom du concours
        :type nomC: String

        :return: la date d'aujourd'hui normalisé pour le nom du pdf
        :rtype: String
    """
    date = datetime.datetime.now()
    return str(date.year)+"_"+str(date.month)+"_"+str(date.day)+"_"+nomC.replace(" ","_")+".pdf"

def enregistrer_nouvelle_equipe(form, idConcours):
    """
        Enregistre une nouvelle équipe dans la table EQUIPE.

        :param form: formulaire NouvelleEquipe
        :param idConcours: l'identifiant du concours actuel
        :type form: Object
        :type idConcours: int
    """
    if form.avatar.data != None:
        avatar = form.avatar.data.filename
    else:
        avatar = None
    equipe = Equipe(avatar=avatar, nomE=form.nom.data, poste=form.poste.data, idC=idConcours)
    db.session.add(equipe)
    db.session.commit()


def modifier_equipe(form, idConcours):
    """
        Modifier une équipe dans la table EQUIPE.

        :param form: formulaire NouvelleEquipe
        :param idConcours: l'identifiant du concours actuel
        :type form: Object
        :type idConcours: int
    """
    idEquipe = form.idEquipe.data
    equipe = get_Equipe(idEquipe)
    equipe.nomE = form.nom.data
    equipe.poste = form.poste.data
    if form.avatar.data != None:
        equipe.avatar = form.avatar.data.filename
    db.session.commit()


def enregistrer_nouveau_joueur(form, idConcours):
    """
        Enregistre une nouvelle équipe dans la table EQUIPE.

        :param form: formulaire NouvelleEquipe
        :param idConcours: l'identifiant du concours actuel
        :type form: Object
        :type idConcours: int
    """
    index = form.nom.data.index(" ")
    nom = form.nom.data[:index].capitalize()
    prenom = form.nom.data[index+1:].capitalize()
    idEquipe = form.equipe.data
    new_Joueur(nom, prenom, idConcours, idEquipe)

def allowed_file(filename):
    """
        retourne si le fichier est dans les types de fichier autorisé dans l'application

        :param filename: le nom du fichier
        :type filename: String

        :return: si le fichier est autorisé
        :rtype: Boolean
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config["ALLOWED_EXTENSIONS"]


def modifier_application_actuelle(form, idApplication):
    """
        Eregistre les modifications qui ont été apporté à
        la table APPLICATION.

        :param form: formulaire ModifierApplication
        :param idApplication: id de l'application
        :type idApplication: int
        :type form: Object
    """
    application = get_Application(idApplication)
    application.nomA = form.nom.data
    if form.logo.data != None and allowed_file(form.logo.data.filename):
        filename = secure_filename(form.logo.data.filename)
        form.logo.data.save(os.path.join("module/static/",app.config["DIRECTORY_THEME_IMAGE"], filename))
        application.logoA = filename
    if form.fond.data != None and allowed_file(form.fond.data.filename):
        filename = secure_filename(form.fond.data.filename)
        form.fond.data.save(os.path.join("module/static/",app.config["DIRECTORY_THEME_IMAGE"], filename))
        application.backgroundConnexionA = filename
    db.session.commit()

def nouvelle_application(form):
    """
        Eregistre une nouvelle application dans la table Application

        :param form: formulaire NouvelleApplication
        :param idApplication: id de l'application
        :type idApplication: int
        :type form: Object
    """
    nomNew = form.nom.data

    if form.logo.data != None:
        logoNew = form.logo.data.filename
    else:
        logoNew = None

    if form.fond.data != None:
        fondNew = form.fond.data.filename
    else:
        fondNew = None

    appli = new_Application(nomNew, fondNew, logoNew)

def lancerConcours(concours, form):
    """
        lance le serveur avec les parametres adéquats et retourne les résultats
        du match

        :param concours: le concours courant
        :param form: le formulaire du concours
        :type concours: Object
        :type repertoire: Object

        :return: les donnees du matchs
        :rtype: dict
    """
    equipes = form.equipe.raw_data
    carte = form.carte.data
    info = dict()
    info["equipes"] = dict()
    urlcarte = copy(concours.repertoireCarte)
    if(urlcarte[-1] != "/"):
        urlcarte += "/"
    info["parametres"] = {"nom":concours.nomC, "map":urlcarte + carte, "n_tours":concours.tempsMatch, "port":concours.port}
    if(len(concours.matchs) == 0 or concours.matchs[-1].phase == "Terminé"):
        new_Match(concours.idC)
        idMatch = concours.matchs[-1].idM
        for idE in equipes:
            new_Effectue(idMatch, idE)
    for idE in equipes:
        e = get_Equipe(idE)
        info["equipes"][e.nomE] = {"id": e.idE, "nom":e.nomE, "machine":e.poste, "avatar":e.avatar}

    parametre = StringIO()
    json.dump(info, parametre, ensure_ascii=True)
    commande = ["python3", concours.commandeLancement]
    try:
        out = subprocess.check_output(commande, input=bytes(parametre.getvalue(), encoding="utf-8")) # Execution du serveur de jeu
    except ValueError:
        print("[ ERROR ] execution lancement du serveur de jeu", ValueError)
    res = json.loads(("{"+str(out).split('{')[-1]).split("}")[0]+"}")
    return res


def convertionDate(dateStr, time):
    """
    crée un datetime grâce à une date en str au format 'dd/mm/yyyy' et un datetime.time
    """
    date = datetime.datetime(
        int(dateStr[-4:]), int(dateStr[5:7]), int(dateStr[:2]),
        time.hour, time.minute)
    return date

def enregistrer_nouveau_concours(form,nomfichier):
    """
        Enregistre une nouvelle équipe dans la table CONCOURS.

        :param form: formulaire NouveauConcours
        :param nomfichier: le nom du fichier pour le sujet
        :type form: Object
        :type nomfichier: String
    """
    nom = form.nom.data
    if form.avatar.data != None:
        image = form.avatar.data.filename
    else:
        image = None

    dateDebut = convertionDate(form.dateDebut.data, form.heureDebut.data)
    dateFin = convertionDate(form.dateFin.data, form.heureFin.data)
    prixInscription = form.participation.data

    if form.sujetConcours.data != None:
        sujet = nomfichier
    else:
        sujet = None

    if form.nbrEquipeParMatch.data == "":
        nbrEquipeParMatch=0
    else:
        nbrEquipeParMatch = form.nbrEquipeParMatch.data
    temps = form.tempsMatch.data
    commandeLancement = form.commandeLancement.data
    port = form.portServeur.data
    repertoireCartes = form.emplacementCarte.data
    new_Concours(nom, image, dateDebut, dateFin, prixInscription, sujet, nbrEquipeParMatch, temps, commandeLancement, port, repertoireCartes)

def enregistrer_modif_concours(form,nomfichier):
    """
        Enregistre une nouvelle équipe dans la table CONCOURS.

        :param form: formulaire NouveauConcours
        :param nomfichier: le nom du fichier pour le sujet
        :type form: Object
        :type nomfichier: String
    """
    concours = get_Concours(form.idC.data)

    concours.nomC = form.nom.data
    if form.avatar.data != None:
        concours.image = form.avatar.data.filename

    if(form.dateDebut.data!=None and form.heureDebut.data!=None):
        concours.dateDeb = convertionDate(form.dateDebut.data, form.heureDebut.data)
    elif form.heureDebut.data!=None:
        concours.dateDeb = concours.dateDeb.replace(hour=form.heureDebut.data.hour, minute=form.heureDebut.data.minute)
    if(form.dateFin.data!=None and form.heureFin.data!=None):
        concours.dateFin = convertionDate(form.dateFin.data, form.heureFin.data)

    concours.prixInscription = form.participation.data

    if nomfichier != None:
        concours.sujetC = nomfichier

    concours.nbrEquipeParMatch = form.nbrEquipeParMatch.data
    concours.tempsMatch = form.tempsMatch.data
    concours.commandeLancement = form.commandeLancement.data
    concours.port = form.portServeur.data
    concours.repertoireCarte = form.emplacementCarte.data
    db.session.commit()
