#### HEAD ##############
# Class   : Effectue
# Content : Table d'association entre MATCH et EQUIPE de type
#           Many-to-Many


#### IMPORT ############
from ..app import db


#### CLASS #############
class Effectue(db.Model):
    idM     = db.Column(db.Integer,db.ForeignKey("match.idM"), primary_key=True)
    idE     = db.Column(db.Integer,db.ForeignKey("equipe.idE"), primary_key=True)
    score   = db.Column(db.Integer)

    def __repr__(self):
        return "<Effectue (%d, %d) %s>" % (self.idE, self.idM, self.score)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

#### GETTEURS ##########
def getAll_Effectue():
    """
        Renvoie toute la table Effectue

        :return: La liste de toutes les valeurs de la table
        :rtype: List
    """
    return Effectue.query.all()


def get_Effectue(idM, idE):
    """
        Renvoie le l'occurence spécifique à l'identifiant

        :param idM: L'identifiant du match
        :param idE: L'identifiant d'une équipe
        :type idM: int
        :type idE: int
        :return: Si l'occurence de la table
        :rtype: Object
    """
    return Effectue.query.filter_by(idM=idM, idE=idE).first()


def getAll_Effectue_by_idMatch(idM):
    """
        Renvoie la liste des objets dont l'identifiant du match
        est donné en paramètre

        :param idM: L'identifiant d'un match
        :type idM: int
        :return: La liste des association entre match et équipe
        :rtype: list
    """
    return Effectue.query.filter_by(idM=idM).all()


def getAll_Effectue_by_idEquipe(idE):
    """
        Renvoie la liste des objets dont l'identifiant de l'équipe
        est donné en paramètre

        :param idE: L'identifiant d'une équipe
        :type idE: int
        :return: La liste des association entre match et équipe
        :rtype: list
    """
    return Effectue.query.filter_by(idE=idE).all()


#### ADD ############
def new_Effectue(idM, idE):
    """
        Ajoute une nouvelle occurence dans la base de données.

        :param idM: L'identifiant du match
        :param idE: L'identifiant d'une équipe
        :type idM: int
        :type idE: int
        :return: Si l'oppération c'est bien déroulé
        :rtyoe: boolean
    """
    try:
        occurence = Effectue(idM=idM, idE=idE, score=0)
        db.session.add(occurence)
        db.session.commit()
        return True
    except:
        return False
