#### HEAD ##############
# Class   : Match
# Content : Table MATCH


#### IMPORT ############
from ..app import db

class Match(db.Model):
    idM         = db.Column(db.Integer, primary_key=True, autoincrement=True)
    idC         = db.Column(db.Integer, db.ForeignKey("concours.idC"), nullable=False)
    phase       = db.Column(db.String(50))
    effectues   = db.relationship("Effectue", backref="match", cascade="all, delete-orphan")

    @property
    def equipes(self):
        """
            Retourne la liste des équipes participant au match
            :return: la liste des équipes participant au match
            :rtyoe: liste d'objets
        """
        res=[]
        for e in self.effectues:
            res.append(e.equipe)
        return res

    def __repr__(self):
        return "<Match (%d) %s>" % (self.idM, self.phase)

    def delete(self):
        """
            Permet la supression de l'objet
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False

#### GETTEURS ##########
def getAll_Match():
    """
        Renvoie toute la table

        :return: La liste de toutes les valeurs de la table
        :rtype: list
    """
    return Match.query.all()

def get_Match(idM):
    """
        Renvoie le match spécifique à l'identifiant

        :param idM: L'identifiant du match
        :type idM: int
        :return: Un match
        :rtype: Object
    """
    return Match.query.get(idM)


#### ADD ############
def new_Match(idC):
    """
        Ajoute une nouvelle l'occurence.

        :param idC: L'identifiant d'un concours
        :type idC: int
        :return: Si l'oppération c'est bien déroulé
        :rtype: boolean
    """
    try:
        occurence = Match(idC=idC, phase="En cours")
        db.session.add(occurence)
        db.session.commit()
        return True
    except:
        return False
