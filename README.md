# Cyber Monsieur Loyal
L’objectif de ce projet consiste a d'evelopper un outil de gestion à destination des organsateurs de tournois de programmation à l'IUT informatique. Il a pour objctif d'optimiser le temps passé à réaliser la composition des équipes qui doivent s'affronter.


### A. Membres du Projet
#### 1. Les Etudiants
- ASTOLFI Anthony
- BERNARD Quentin
- HENRIQUES Alan
- ORLAY Arnaud
- PIROIRD Romain
- TISSIER Dorian
- VERNADAT Mickaël
- VILLEBONNET Bastien

#### 2. Les Enseignants
- BROSIER Nathalie
- BECKER Florent


### B. Technologies
- `Flask` ([documentation](http://flask.pocoo.org/docs/1.0/quickstart/))
- `Materialize` ([documentation](https://materializecss.com/))
- `jQuery` ([documentation](https://api.jquery.com/))


### C. Installation
```bash
git clone https://gitlab.com/IUT-2019/cyber-monsieur-loyal.git
cd cyber-monsieur-loyal
./setup.sh # Executer le fichier d'installation
```

### D. Execution
```bash
./run.sh # Exucuter le fichier de lancement de l'application
# L'application est accesible sur http://localhost:5000
```


### F. Proxy
Si vous êtes derrière un proxy, n'oubliez pas de renseigner la variable d'environnement `HTTP_PROXY` (en majuscules).