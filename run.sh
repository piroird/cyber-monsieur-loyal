#! /bin/bash

# Lancement de l'application
gunicorn --certfile module/server/cert.pem --keyfile module/server/key.pem -b localhost:5000 module:app
